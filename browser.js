if (!window.WebSocket) {
    document.body.innerHTML = 'WebSocket в этом браузере не поддерживается.';
}

var myName = '';
var myID;
var MyStatus = 'v';
// создать подключение
var socket;
var TurnXO;


function startX0() {

    socket = new WebSocket("ws://127.0.0.1:8081");


//socket.send(':|::|:'+outgoingMessage);

// обработчик входящих сообщений
    socket.onmessage = function (event) {
        var incomingMessage = JSON.parse(event.data);
        //message = JSON.parse(incomingMessage);
        //console.log("" + incomingMessage.toString());
        renderAllcomponents(incomingMessage);

    };

}

function renderAllcomponents(incData) {
    //incData.userID ='';
    //incData.usersName

    //incData.onlineUsers
    //incData.gameData
    myID = incData.userID;
    TurnXO = incData.TurnXO;
    setClientsCount(incData.onlineUsers);
    printUserStatus(incData.userX, incData.userO);
    printGameTable(incData.gameData);
    SetVictory(incData.VictoryFlag);
    whoTurn();
}

function SetVictory(VictoryFlag) {
    if (VictoryFlag != '') {
        if (MyStatus != 'v') {
            if (VictoryFlag == MyStatus) {
                VictoryWin();
            } else if (VictoryFlag == 'xo') {
                StandoffWin();
            } else {
                DefeatWin();
            }
        }
    }
}


function printUserStatus(userX, userO) {
    if (userX == myID) {
        userStatusM = 'Вы играете крестиком';
        MyStatus = 'x';
    } else if (userO == myID) {
        userStatusM = 'Вы играете ноликом';
        MyStatus = 'o';
    } else {
        userStatusM = 'Вы в очереди на игру';
    }


    $('#userStatus').html(userStatusM);

}

function myXod(Row, Cell) {
    outmess = {
        userID: myID,
        setapUser: [Row, Cell, MyStatus],
    };
//clients[keys].send(JSON.stringify(outmess));
    //alert(outmess.setapUser+'');
    socket.send(JSON.stringify(outmess));
}

function printRow(ArrData, id) {
    HTMLPattern = '<div class="chatR">';
    for (var i = 0; i < ArrData.length; i++) {
        if (ArrData[i] == 0) {
            ElClass = 'none';
            text = '';
            if (MyStatus != 'v') {
                if (MyStatus == 'x') {
                    if (TurnXO == 0) {
                        elOnclick = 'onclick="myXod(' + id + ',' + i + ');" ';
                    } else {
                        elOnclick = '';
                    }
                } else {
                    if (TurnXO == 1) {
                        elOnclick = 'onclick="myXod(' + id + ',' + i + ');" ';
                    } else {
                        elOnclick = '';
                    }

                }
            } else {
                elOnclick = '';
            }

        }
        if (ArrData[i] == 'x') {
            ElClass = 'x';
            text = 'x';
            elOnclick = '';
        }
        if (ArrData[i] == 'o') {
            ElClass = 'o';
            text = 'o';
            elOnclick = '';
        }


        HTMLPattern += '<div ' + elOnclick + 'class="chatC"><div class="elem ' + ElClass + '">' + text + '</div></div>';
    }
    HTMLPattern += '</div>';

    return HTMLPattern;
}

function printGameTable(ArrData) {
    HTMLPattern = '<div class="chatT">';
    for (var i = 0; i < ArrData.length; i++) {
        HTMLPattern += '' + printRow(ArrData[i], i);
    }
    HTMLPattern += '</div>';

    $('#gameRender').html(HTMLPattern);

}

function whoTurn() {

    htmml = TurnXO ? 'Ходят Нолики' : 'Ходят Крестики';
    $('#whoTurn').html(htmml);
}

function EnterMessegeSender(e) {
    if (e.keyCode == 13) {
        MessegeSender();
        return false;
    }
}

function MessegeSender() {

    if (Cookie.exists('userName') == false && myName != '') {
        modalWindowAUTH();
    } else {
        outgoingMessage = $('#message').val();
        if (outgoingMessage != '') {
            $('#message').val('');
            socket.send(myName + ':|:' + outgoingMessage);
        }
    }

    /*
     document.forms.publish.onsubmit = function() {
     var outgoingMessage = this.message.value;
     this.message.value = '';
     socket.send(myName+':|:'+outgoingMessage);
     return false;
     };
     */
}

function setClientsCount(count) {
    //clients[key].send(':|:countUsers:|:'+clients.length);
    $('#onlineUsers').html(count + ' чел.');
}


//------------------------------MODAL WIN-----------------------------------

function VictoryWin() {
    ModWinLoading();
    var HTMLpattern = "";
    setTimeout(function () {
        HTMLpattern = '<div class="ok">Ура!!! Ты одержал(а) победу</div>';
        ModalWinPettern('Победа!', HTMLpattern);

    }, 1500);
}

function StandoffWin() {
    ModWinLoading();
    var HTMLpattern = "";
    setTimeout(function () {
        HTMLpattern = '<div class="error">Победитель не определен, игайте заново</div>';
        ModalWinPettern('Ничья!', HTMLpattern);

    }, 1500);
}

function DefeatWin() {
    ModWinLoading();
    var HTMLpattern = "";
    setTimeout(function () {
        HTMLpattern = '<div class="error">Ты проиграл(а), теперь будешь перемещен(а) в конец очереди</div>';
        ModalWinPettern('Поражение!', HTMLpattern);

    }, 1500);
}

function modalWindowCl() {
    $('#ModWinBack').html('');
    $('#ModWinBack').fadeOut();
}
function ModalWinPettern(WinTitle, WinBody) {
    WinTitle = WinTitle ? WinTitle : '';
    WinBody = WinBody ? WinBody : '';

    var HTMLpattern = "";

    HTMLpattern = '' +
        '<div id="ModWin">' +
        '<div class="ModWinBody">' +
        '<div class="WinTitle">' + WinTitle + '</div>' +
        '<div class="closeBtn" onclick="modalWindowCl(); return false;"></div>' +
        '<div class="WinBody">' + WinBody + '</div>' +
        '</div>' +
        '</div>' +
        '';
    $('#ModWinBack').html(HTMLpattern);
    $('#ModWinBack').fadeIn(1500);
    ModWinCenter();

}
function ModWinCenter() {
    if ($('#ModWinBack').width() <= 700) {
        winWidthCen = 0;
        winHeightCen = 0;
        styles = {
            top: "0px",
            left: "0px",
        };
        //$('#ModWinBack').css( {position: "absolute" } );
    } else {
        styles = {
            top: ($('#ModWinBack').height() / 2) - ($('#ModWin').height() / 2) + "px",
            left: ($('#ModWinBack').width() / 2) - ($('#ModWin').width() / 2) + "px"
        };
    }
    $('#ModWin').css(styles);
}
function ModWinLoading() {
    ModalWinPettern('', '<div class="loadingF"><div class="loading"></div><div class="loadingafter">Ожидайте</div></div>');
}

function modalWindowAUTH() {

    ModWinLoading();
    var HTMLpattern = "";
    setTimeout(function () {
        HTMLpattern = '' +
            '<div class="Zakazfio"><input name="fio" id="Zakazfio" placeholder="Твоё имя" type="text"></div>' +
            '<div class="forBtn"><a class="buttonGreenBig" title="" onclick="UserAuth(); return false;" href="#" >Войти в чат </a></div>' +
            '';
        ModalWinPettern('Авторизация', HTMLpattern);

    }, 1500);


}

function UserAuth() {

    Zakazfio = $('#Zakazfio').val();
    if (Zakazfio == "") {
        $('#Zakazfio').css('border', 'solid 1px red'); //norequired
        $('.Zakazfio').addClass('norequired');
        setTimeout(function () {
            $('.Zakazfio').removeClass('norequired');
        }, 3000);

        return;
    } else {
        ModWinLoading();
        today = new Date();
        today_date = today.getDate();
        today.setDate(today_date + 1);
        Cookie.write('userName', Zakazfio + '', today);

        setTimeout(function () {
            modalWindowCl();
        }, 2000);
        myName = Zakazfio;
        startX0();
    }


}


//--------------------------------------------------------------------------


//-----------------------------Cookie lib-----------------------------//
var Cookie = {
    isSupported: function () {
        return !!navigator.cookieEnabled;
    },
    exists: function (name) {
        return document.cookie.indexOf(name + "=") + 1;
    },
    write: function (name, value, expires, path, domain, secure) {
        expires instanceof Date ? expires = expires.toGMTString()
            : typeof(expires) == 'number' && (expires = (new Date(+(new Date) + expires * 1e3)).toGMTString());
        var r = [name + "=" + escape(value)], s, i;
        for (i in s = {expires: expires, path: path, domain: domain})
            s[i] && r.push(i + "=" + s[i]);
        return secure && r.push("secure"), document.cookie = r.join(";"), true;
    },
    read: function (name) {
        var c = document.cookie, s = this.exists(name), e;
        return s ? unescape(c.substring(s += name.length, (c.indexOf(";", s) + 1 || c.length + 1) - 1)) : "";
    },
    remove: function (name, path, domain) {
        return this.exists(name) && this.write(name, "", new Date(0), path, domain);
    }
};

//---------------------------------------------------------------------------------------------
window.onload = function () {
    if (Cookie.exists('userName') == false) {
        modalWindowAUTH();
    } else {
        userName = Cookie.read('userName');
        //alert(userName);
        myName = userName;
        startX0();
    }
};

