var http = require('http');
var Static = require('node-static');
var WebSocketServer = new require('ws');


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//Удаление юзера из очереди 
function turnOnGameRemoveID(id) {
    for (var i = 0; i < turnOnGame.length; i++) {
        if (id == turnOnGame[i]) {
            turnOnGame.splice(i, 1);
        }
    }
}

//Вставить юзера в конец очереди
function turnOnGamePushID(id) {
    turnOnGame.push(id);
}

function turnOnGameToEnd(id) {
    turnOnGameRemoveID(id);
    turnOnGame.push(id);
}

function setGamedata(arr) {
    if (arr[2] == 'x') {
        TurnXO = 1;
    } else {
        TurnXO = 0;
    }
    //[Row,Cell,MyStatus]
    CountProgress++;
    gameData[arr[0]][arr[1]] = arr[2];

    if (arr[2] == 'x') {
        if (getXVictory()) {
            VictoryFlag = 'x';
        }
    } else {
        if (getOVictory()) {
            VictoryFlag = 'o';
        }
    }

    if (CountProgress == 9) {
        if (VictoryFlag != 'o' & VictoryFlag != 'x') {
            VictoryFlag = 'xo';
        }
    }


}

function VictoryDO() {
    setTimeout(function () {

        tempVictoryFlag = VictoryFlag;
        if (tempVictoryFlag == 'xo') {

        } else if (tempVictoryFlag == 'o') {
            turnOnGameToEnd(turnOnGame[0]);
        } else if (tempVictoryFlag == 'x') {
            turnOnGameToEnd(turnOnGame[1]);
        }

        VictoryFlag = '';
        CountProgress = 0;
        gameData = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
        TurnXO = 0;
        for (var keys in clients) {
            outmess = {
                userID: keys,
                usersName: '',
                userX: turnOnGame[0],
                userO: turnOnGame[1] ? turnOnGame[1] : 0,
                onlineUsers: CountUsers,
                gameData: gameData,
                VictoryFlag: VictoryFlag,
                TurnXO: TurnXO
            };
            clients[keys].send(JSON.stringify(outmess));
        }
    }, 1000);
}

function getXVictory() {
    return getVictory('x');
}
function getOVictory() {
    return getVictory('o');
}

function getVictory(WHO) {
    WHO = WHO == 'x' ? 'x' : 'o';


    var tempArr = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
    for (var i = 0; i < gameData.length; i++) {
        for (var ii = 0; ii < gameData[i].length; ii++) {
            if (gameData[i][ii] == WHO) {
                tempArr[i][ii] = 1;
            }
        }
    }

    //Грубая проверка на предмет победы
    if (tempArr[0][0] & tempArr[0][1] & tempArr[0][2]) {
        return true;
    } else if (tempArr[1][0] & tempArr[1][1] & tempArr[1][2]) {
        return true;
    } else if (tempArr[2][0] & tempArr[2][1] & tempArr[2][2]) {
        return true;
    } else if (tempArr[0][0] & tempArr[1][0] & tempArr[2][0]) {
        return true;
    } else if (tempArr[0][1] & tempArr[1][1] & tempArr[2][1]) {
        return true;
    } else if (tempArr[0][2] & tempArr[1][2] & tempArr[2][2]) {
        return true;
    } else if (tempArr[0][0] & tempArr[1][1] & tempArr[2][2]) {
        return true;
    } else if (tempArr[0][2] & tempArr[1][1] & tempArr[2][0]) {
        return true;
    } else {
        return false;
    }


}


//var getUserMedia = require('getusermedia');

// подключенные клиенты
var clients = {};
var CountUsers = 0;
var userStatus = '';
var userX = 0;
var userO = 0;
var turnOnGame = [];
var gameData = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
var VictoryFlag = '';
var TurnXO = 0;
var CountProgress = 0;


// WebSocket-сервер на порту 8081
var webSocketServer = new WebSocketServer.Server({port: 8081});
webSocketServer.on('connection', function (ws) {
    CountUsers++;
    var id = getRandomInt(11111111111111, 99999999999999);
    clients[id] = ws;
    turnOnGamePushID(id);

    console.log("новое соединение " + id);
    console.log('В чате: ' + CountUsers + ' чел.');


    for (var keys in clients) {
        outmess = {
            userID: keys,
            usersName: '',
            userX: turnOnGame[0],
            userO: turnOnGame[1] ? turnOnGame[1] : 0,
            onlineUsers: CountUsers,
            gameData: gameData,
            VictoryFlag: VictoryFlag,
            TurnXO: TurnXO
        };
        clients[keys].send(JSON.stringify(outmess));
    }


    ws.on('message', function (message) {
        /*gameData
         outmess =  {
         userID: myID,
         setapUser: [Row,Cell,MyStatus],
         };
         */
        var incData = JSON.parse(message);
        setGamedata(incData.setapUser);
        for (var keys in clients) {
            outmess = {
                userID: keys,
                usersName: '',
                userX: turnOnGame[0],
                userO: turnOnGame[1] ? turnOnGame[1] : 0,
                onlineUsers: CountUsers,
                gameData: gameData,
                VictoryFlag: VictoryFlag,
                TurnXO: TurnXO
            };
            clients[keys].send(JSON.stringify(outmess));
        }

        if (VictoryFlag != '') {
            VictoryDO();
        }

    });


    ws.on('close', function () {
        console.log('соединение закрыто ' + id);
        CountUsers--;
        turnOnGameRemoveID(id);
        console.log('В чате: ' + CountUsers + ' чел.');
        console.log('turnOnGame: ' + turnOnGame + '');
        delete clients[id];
        for (var keys in clients) {
            outmess = {
                userID: keys,
                usersName: '',
                userX: turnOnGame[0],
                userO: turnOnGame[1] ? turnOnGame[1] : 0,
                onlineUsers: CountUsers,
                gameData: [[0, 0, 'x'], [0, 'o', 'o'], [0, 0, 0]]
            };
            clients[keys].send(JSON.stringify(outmess));
        }


    });


});


// обычный сервер (статика) на порту 8080
var fileServer = new Static.Server('.');
http.createServer(function (req, res) {

    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress;
    console.log("Входящий http " + ip);

    fileServer.serve(req, res);

}).listen(8080);

console.log("Сервер запущен на портах 8080, 8081");

